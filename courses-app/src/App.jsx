import { useState } from 'react';
import './App.css';
import Courses from './components/Courses/Courses';
import CreateCourse from './components/CreateCourse/CreateCourse';
import Header from './components/Header/Header';
import { mockedAuthorsList, mockedCoursesList } from './constants';

function App(props) {
	const [coursesList, setCoursesList] = useState(mockedCoursesList);
	const [isCourseCreationMode, setCourseCreationMode] = useState(false);
	const [allAuthorsList, setAllAuthorsList] = useState(mockedAuthorsList);

	function toggleMode() {
		setCourseCreationMode(!isCourseCreationMode);
	}

	return (
		<div className='wrapper'>
			<Header />
			{isCourseCreationMode ? (
				<CreateCourse
					setCoursesList={setCoursesList}
					coursesList={coursesList}
					toggleMode={toggleMode}
					allAuthorsList={allAuthorsList}
					setAllAuthorsList={setAllAuthorsList}
				/>
			) : (
				<Courses
					coursesList={coursesList}
					toggleMode={toggleMode}
					allAuthorsList={allAuthorsList}
				/>
			)}
		</div>
	);
}

export default App;
