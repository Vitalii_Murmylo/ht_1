import s from './Input.module.css';

function Input(props) {
	return (
		<>
			<label htmlFor={props.inputId}>{props.labelText}</label>
			<input
				onChange={props.onChange}
				id={props.inputId}
				type='text'
				className={s.input}
				placeholder={props.placeholder}
				value={props.value}
			/>
		</>
	);
}

export default Input;
