import s from './Logo.module.css';
import logo from './logo.jpg';

function Logo() {
	return <img className={s.logo} src={logo} alt='Logo' />;
}

export default Logo;
