import Button from '../../common/Button/Button';
import Logo from './Logo/Logo';

import s from './Header.module.css';

function Header() {
	return (
		<header className={s.header}>
			<Logo />
			<div className={s.nameAndLogout}>
				<div className={s.name}>Chell</div>
				<Button buttonText='Logout' />
			</div>
		</header>
	);
}

export default Header;
