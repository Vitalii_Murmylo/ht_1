import Button from '../../../../common/Button/Button';
import Input from '../../../../common/Input/Input';
import s from './SearchBar.module.css';

function SearchBar(props) {
	return (
		<div className={s.searchBar}>
			<Input placeholder='Enter course name...' onChange={props.onChange} />
			<Button buttonText='Search' onClick={props.onClick} />
		</div>
	);
}

export default SearchBar;
