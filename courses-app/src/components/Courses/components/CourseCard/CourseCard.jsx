import Button from '../../../../common/Button/Button';
import { transformCourseDurationIntoString } from '../../../../helpers/pipeDuration';
import s from './CourseCard.module.css';

function CourseCard(props) {
	return (
		<div className={s.courseCard}>
			<div className={s.courseTitleAndDescription}>
				<div>
					<h2>{props.title}</h2>
				</div>
				<div>
					<p>{props.description}</p>
				</div>
			</div>
			<div className={s.courseInfo}>
				<p className={s.authors}>
					<b>Authors: </b>
					{props.authors.join(', ')}
				</p>
				<p className={s.duration}>
					<b>Duration: </b>
					{transformCourseDurationIntoString(props.duration)}
				</p>
				<p className={s.created}>
					<b>Created: </b>
					{props.created.split('/').join('.')}
				</p>
				<div className={s.showCourse}>
					<Button buttonText='Show course' />
				</div>
			</div>
		</div>
	);
}

export default CourseCard;
