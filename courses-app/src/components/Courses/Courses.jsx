import s from './Courses.module.css';

import CourseCard from './components/CourseCard/CourseCard';
import SearchBar from './components/SearchBar/SearchBar';
import Button from '../../common/Button/Button';
import { useState } from 'react';

function Courses(props) {
	const [searchValue, setSearchValue] = useState('');

	const coursesCards = props.coursesList.map((course) => {
		let authorsNames = course.authors.map((authorId) => {
			let authorName;
			props.allAuthorsList.forEach((authorObject) => {
				if (authorObject.id === authorId) {
					authorName = authorObject.name;
				}
			});
			return authorName;
		});
		return (
			<CourseCard
				key={course.id}
				title={course.title}
				description={course.description}
				authors={authorsNames}
				duration={course.duration}
				created={course.creationDate}
			/>
		);
	});

	const [filteredCards, setFilteredCards] = useState(coursesCards);

	function showFilteredCards(input) {
		setFilteredCards(
			coursesCards.filter(
				(course) =>
					course.props.title.toLowerCase().indexOf(input.toLowerCase()) !== -1
			)
		);
	}

	let onInputChange = (e) => {
		setSearchValue(e.target.value);
		if (searchValue === '') {
			setFilteredCards(coursesCards);
		}
	};

	return (
		<main className={s.courses}>
			<div className={s.searchBarAndButton}>
				<SearchBar
					onClick={() => showFilteredCards(searchValue)}
					onChange={onInputChange}
				/>
				<Button buttonText='Add new course' onClick={props.toggleMode} />
			</div>
			{searchValue === '' ? coursesCards : filteredCards}
		</main>
	);
}

export default Courses;
